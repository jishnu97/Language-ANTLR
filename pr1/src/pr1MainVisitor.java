import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class pr1MainVisitor extends pr1BaseVisitor<Object> {
    private BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    private boolean inNested = false;

    private HashMap<String, Object> globalSymbolTable = new HashMap<String, Object>();
    private HashMap<String, Object> symbolTable = globalSymbolTable;
    private HashMap<String, pr1Parser.DeclSubContext> subTable = new HashMap<String, pr1Parser.DeclSubContext>();
    private ArrayList<String> activeSubroutines = new ArrayList<String>();

    private class SubException extends RuntimeException {
        private Number value = null;

        public SubException(Number n) { value = n; }

        public Number getValue() { return value; }
    }

    private String getSubSignature(pr1Parser.DeclSubContext ctx) {
        String n = ctx.sub_head().ID().getText() +"(";
        for (pr1Parser.ParamContext p: ctx.sub_head().param_list().param()) {
            n += " " + p.type().getText();
        }
        n += " )";
        return n;
    }

    private String getSubSignature(pr1Parser.FactorSubContext ctx) {
        String n = ctx.ID().getText() +"(";
        for (pr1Parser.ExprContext e: ctx.expr_list().expr()) {
            n += " " + (isInteger((Number)this.visit(e)) ? "int" : "real");
        }
        n += " )";
        return n;
    }

    @Override public Boolean visitBF(pr1Parser.BFContext ctx)
    {
        return false;
    }

    @Override public Boolean visitBoolRelOp(pr1Parser.BoolRelOpContext ctx)
    {
        Number num1 = (Number) this.visit(ctx.e1);
        Number num2 = (Number) this.visit(ctx.e2);
        if(ctx.REL().getText().equals("<"))
        {
            return num1.floatValue() < num2.floatValue();
        }
        if(ctx.REL().getText().equals("<="))
        {
            return num1.floatValue() <= num2.floatValue();
        }
        if(ctx.REL().getText().equals("=="))
        {
            return num1.floatValue() == num2.floatValue();
        }
        if(ctx.REL().getText().equals("!="))
        {
            return num1.floatValue() != num2.floatValue();
        }
        if(ctx.REL().getText().equals(">="))
        {
            return num1.floatValue() >= num2.floatValue();
        }
        if(ctx.REL().getText().equals(">"))
        {
            return num1.floatValue() > num2.floatValue();
        }
        printError("Unsupported");
        return null;
    }

    @Override public Boolean visitHandleParanthesis(pr1Parser.HandleParanthesisContext ctx)
    {
        return (Boolean) this.visit(ctx.bool());
    }

    @Override public Boolean visitAssBool(pr1Parser.AssBoolContext ctx)
    {
        String id = ctx.ID().getText();
        Boolean value = (Boolean) checkIdValue(id);
        Boolean e = (Boolean) this.visit(ctx.bool());
        if (checkTypeMatch(value, e)) {
            symbolTable.replace(id, value, e);
        }
        return null;
    }

    @Override public Object visitStmtAssignId(pr1Parser.StmtAssignIdContext ctx)
    {
        Object obj1 = checkIdValue(ctx.a.getText());
        Object obj2 = checkIdValue(ctx.b.getText());
        if(checkTypeMatch(obj1, obj2))
        {
            symbolTable.replace(ctx.a.getText(), obj1, obj2);
        }
        return null;
    }

    @Override public Boolean visitWBool(pr1Parser.WBoolContext ctx)
    {
        Boolean value = (Boolean) this.visit(ctx.bool());
        if (value != null) {
            System.out.println(value.booleanValue());
        }
        return null;
    }

    @Override public Object visitWriteId(pr1Parser.WriteIdContext ctx)
    {
            if(checkIdValue(ctx.ID().getText()).getClass() == Boolean.class)
            {
                System.out.println(((Boolean)checkIdValue(ctx.ID().getText())).booleanValue());
            }
            else
            {
                if (isFloat((Number)checkIdValue(ctx.ID().getText())))
                {
                    System.out.println(((Number)checkIdValue(ctx.ID().getText())).floatValue());
                }
                else
                {
                    System.out.println(((Number)checkIdValue(ctx.ID().getText())).intValue());
                }
            }
            return null;

    }

    @Override
    public Number visitDeclSub(pr1Parser.DeclSubContext ctx) {
        String n = getSubSignature(ctx);
        pr1Parser.DeclSubContext value = (pr1Parser.DeclSubContext) subTable.get(n);
        if (value == null && ctx != null && n != null) {
            subTable.put(n, ctx);
        }
        else {
            if (value != null) {
                printError("Subroutine " + ctx.sub_head().ID().getText() + " already declared as " + n);
            }
            if (ctx == null) {
                printError("Initializing with null value");
            }
        }
        return null;
    }

    @Override
    public Number visitRet(pr1Parser.RetContext ctx) {
        Number value = (Number) this.visit(ctx.expr());
        if (symbolTable == globalSymbolTable) {
            printError("RETURN statement outside a subroutine");
            return null;
        }
        else {
            throw new SubException(value);
        }
    }

    @Override public Number visitFactorSub(pr1Parser.FactorSubContext ctx) {
        HashMap<String, Object> currentTable = symbolTable;
        String signature = getSubSignature(ctx);
        /*if (activeSubroutines.contains(signature)) {
            printError("Recursive subroutine call: " + signature);
            return null;
        }*/
        activeSubroutines.add(signature);
        pr1Parser.DeclSubContext c = subTable.get(signature);
        Number value = null;
        if (c != null) {
            ArrayList<String> globalVariables = new ArrayList<String>();
            ArrayList<String> currentVariables = new ArrayList<String>();
            try {
                ArrayList<Number> actualValues = new ArrayList<Number>();
                for (pr1Parser.ExprContext e: ctx.expr_list ().expr()) {
                    actualValues.add((Number) this.visit(e));
                }
                symbolTable = new HashMap<String, Object>();
                Iterator<Number> it = actualValues.iterator();
                for (pr1Parser.ParamContext p: c.sub_head().param_list().param()) {
                    declare(it.next(), p.ID().getText());
                }
                for (Map.Entry<String, Object> s: globalSymbolTable.entrySet()) {
                    if (symbolTable.get(s.getKey()) == null) {
                        globalVariables.add(s.getKey());
                        declare((Number) s.getValue(), s.getKey());
                    }
                    if (currentTable.get(s.getKey()) == null) {
                        currentVariables.add(s.getKey());
                    }
                }
                this.visit(c.sub_body());
                printError("No return value");
            }
            catch (SubException e) {
                value = e.getValue();
            }
            for (String v: globalVariables) {
                globalSymbolTable.replace(v, symbolTable.get(v));
            }
            for (String v: currentVariables) {
                currentTable.replace(v, symbolTable.get(v));
            }
        }
        else {
            printError("Undeclared subroutine: " + signature);
        }
        activeSubroutines.remove(signature);
        symbolTable = currentTable;
        return value;
    }

    @Override
    public Number visitDeclVar(pr1Parser.DeclVarContext ctx) {
        switch (ctx.type().getText()) {
            case "int":
                declare(new Integer(0), ctx.ID().getText());
                break;
            case "real":
                declare(new Float(0), ctx.ID().getText());
                break;
            case "bool":
                declare(new Boolean(false), ctx.ID().getText());
                break;
            default:
                printError("Unknown type " + ctx.type().getText());
        }
        return null;
    }

    @Override
    public Number visitStmtAssign(pr1Parser.StmtAssignContext ctx) {
        String id = ctx.ID().getText();
        Number value = (Number) checkIdValue(id);
        Number e = (Number) this.visit(ctx.expr());
        if (checkTypeMatch(value, e)) {
            symbolTable.replace(id, value, e);
        }
        return null;
    }

    @Override public Number visitStmtRead(pr1Parser.StmtReadContext ctx) {
        String id = ctx.ID().getText();
        Number value = (Number) checkIdValue(id);
        if (value != null) {
            try {
                String line = in.readLine();
                Number newValue = null;
                if (isInteger(value)) {
                    newValue = new Integer(Integer.parseInt(line));
                }
                else {
                    newValue = new Float(Float.parseFloat(line));
                }
                symbolTable.replace(id, value, newValue);
            }
            catch (NumberFormatException e) {
                printError("Invalid number format");
            }
            catch (IOException e) {
                printError("Input not available");
            }
        }
        return null;
    }

    @Override public Number visitStmtWrite(pr1Parser.StmtWriteContext ctx) {
        Number value = (Number) this.visit(ctx.expr());
        if (value != null) {
            if (isInteger(value)) {
                System.out.println(value.intValue());
            }
            else {
                System.out.println(value.floatValue());
            }
        }
        return null;
    }

    @Override public Number visitDoLoop(pr1Parser.DoLoopContext ctx)
    {
        String id = ctx.do_head().ID().getText();
        int start = Integer.parseInt(ctx.do_head().INT_CONST(0).getText());
        int end = Integer.parseInt(ctx.do_head().INT_CONST(1).getText());
        int step = ctx.do_head().INT_CONST().size() == 3 ? Integer.parseInt(ctx.do_head().INT_CONST(2).getText()) : 1;
        for (int i = start; i <= end && !inNested; i += step) {
            symbolTable.put(id, new Integer(i));
            this.visit(ctx.stmt_list());
        }
        inNested = false;

        return null;
    }

    @Override public Number visitIfSel(pr1Parser.IfSelContext ctx) {
        Boolean cond = (Boolean) this.visit(ctx.if_cond().bool());

        if (cond.booleanValue())
        {
            this.visit(ctx.if_then().if_body());
        }
        else if (ctx.if_else() != null && !ctx.if_else().isEmpty())
        {
            this.visit(ctx.if_else().if_body());
        }

        inNested = false;
        return null;
    }

    @Override public Number visitExprTerm(pr1Parser.ExprTermContext ctx) { return (Number)this.visit(ctx.term()); }

    @Override public Number visitExprAddOp(pr1Parser.ExprAddOpContext ctx) { return evaluateOp(ctx.add_op().getText(), (Number)this.visit(ctx.expr()), (Number)this.visit(ctx.term())); }

    @Override public Number visitTermFactor(pr1Parser.TermFactorContext ctx) { return (Number)this.visit(ctx.factor()); }

    @Override public Number visitTermMultOp(pr1Parser.TermMultOpContext ctx) { return evaluateOp(ctx.mult_op().getText(), (Number)this.visit(ctx.term()), (Number)this.visit(ctx.factor())); }

    @Override public Number visitFactorExpr(pr1Parser.FactorExprContext ctx) {
        return (Number)this.visit(ctx.expr());
    }

    @Override public Number visitFactorId(pr1Parser.FactorIdContext ctx) { return (Number)checkIdValue(ctx.ID().getText()); }

    @Override public Number visitFactorIntConst(pr1Parser.FactorIntConstContext ctx) { return new Integer(ctx.INT_CONST().getText()); }

    @Override public Number visitFactorRealConst(pr1Parser.FactorRealConstContext ctx) { return new Float(ctx.REAL_CONST().getText()); }

    @Override public Number visitFactorFloat(pr1Parser.FactorFloatContext ctx) {
        Number v = (Number)this.visit(ctx.expr());
        return isInteger(v) ? new Float(v.floatValue()) : null;
    }

    @Override public Number visitFactorTrunc(pr1Parser.FactorTruncContext ctx) {
        Number v = (Number)this.visit(ctx.expr());
        return isFloat(v) ? new Integer(v.intValue()) : null;
    }

    /**
     * Checks if the variable is already declared.
     *
     * @param id The variable name
     * @return The variable value, <code>null</code> if not declared
     */
    private Object checkIdValue(String id) {
        Object value = symbolTable.get(id);
        if (value == null) {
            printError("Variable " + id + " not declared");
        }
        return value;
    }

    /**
     * Tests if two values are of the same type.
     *
     * @param v1 First value
     * @param v2 Second value
     * @return <code>true</code> if values have the same type
     */
    private boolean checkTypeMatch(Object v1, Object v2) {
        boolean check = false;
        if (v1 != null && v2 != null && v1.getClass() == v2.getClass()) {
            check = true;
        }
        else {
            printError("Type mismatch");
        }
        return check;
    }

    /**
     * Declares a new variable and sets the initial value.
     *
     * @param v The initial value
     * @param id The variable name
     */
    private void declare(Object v, String id) {
        Object value = symbolTable.get(id);
        if (value == null && v != null && id != null) {
            symbolTable.put(id, v);
        }
        else {
            if (value != null) {
                printError("Variable " + id + " already declared");
            }
            if (v == null) {
                printError("Initializing with null value");
            }
        }
    }

    /**
     * Tests if the value is <code>Integer</code>.
     *
     * @param v The value
     * @return <code>true</code> if the value is <code>Integer</code>
     */
    private static boolean isInteger(Number v) {
        return (v instanceof Integer);
    }

    /**
     * Tests if the value is <code>Float</code>.
     *
     * @param v The value
     * @return <code>true</code> if the value is <code>Float</code>
     */
    private static boolean isFloat(Number v) {
        return (v instanceof Float);
    }

    /**
     * Performs an arithmetic operation on two operands.
     *
     * @param op The operation to perform
     * @param v1 First operand
     * @param v2 Second operand
     * @return The result of the operation
     */
    private Number evaluateOp(String op, Number v1, Number v2) {
        Number value = null;
        if (checkTypeMatch(v1, v2)) {
            if (isInteger(v1)) {
                switch (op) {
                    case "+":
                        value = new Integer(v1.intValue() + v2.intValue());
                        break;
                    case "-":
                        value = new Integer(v1.intValue() - v2.intValue());
                        break;
                    case "*":
                        value = new Integer(v1.intValue() * v2.intValue());
                        break;
                    case "/":
                        value = new Integer(v1.intValue() / v2.intValue());
                        break;
                    default:
                        printError("Invalid arithmetic operation " + op);
                }
            }
            else if (isFloat(v1)){
                switch (op) {
                    case "+":
                        value = new Float(v1.floatValue() + v2.floatValue());
                        break;
                    case "-":
                        value = new Float(v1.floatValue() - v2.floatValue());
                        break;
                    case "*":
                        value = new Float(v1.floatValue() * v2.floatValue());
                        break;
                    case "/":
                        value = new Float(v1.floatValue() / v2.floatValue());
                        break;
                    default:
                        printError("Invalid arithmetic operation " + op);
                }
            }
            else {
                printError("Invalid type");
            }
        }
        return value;
    }
    @Override public Boolean visitBoolNot(pr1Parser.BoolNotContext ctx)
    {
        return !((Boolean) this.visit(ctx.bool())).booleanValue();
    }

    @Override public Boolean visitBoolOp(pr1Parser.BoolOpContext ctx)
    {
        Boolean a = (Boolean)this.visit(ctx.a);
        Boolean b = (Boolean)this.visit(ctx.b);

        if(ctx.BOP().getText().equals("OR"))
        {
            return a.booleanValue() || b.booleanValue();
        }
        else if(ctx.BOP().getText().equals("AND"))
        {
            return a.booleanValue() && b.booleanValue();
        }
        else
        {
            printError("Unsupported");
        }
        return null;

    }

    /**
     * Pritns the error message and exits.
     *
     * @param s Message to print
     */
    private static void printFatalError(String s) {
        System.err.println("FATAL ERROR: " + s + ".");
        System.exit(1);
    }

    /**
     * Prints the error message.
     *
     * @param s Message to print
     */
    private static void printError(String s) {
        System.err.println("ERROR: " + s + ".");
    }
    @Override public Boolean visitBoolId(pr1Parser.BoolIdContext ctx)
    {
        return (Boolean)checkIdValue(ctx.ID().getText());
    }

    @Override public Boolean visitBT(pr1Parser.BTContext ctx)
    {
        return true;
    }
}