// Generated from /Users/JishnuRenugopal/IdeaProjects/pr1/pr1.g4 by ANTLR 4.6
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link pr1Parser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface pr1Visitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link pr1Parser#program}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgram(pr1Parser.ProgramContext ctx);
	/**
	 * Visit a parse tree produced by {@link pr1Parser#stmt_list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStmt_list(pr1Parser.Stmt_listContext ctx);
	/**
	 * Visit a parse tree produced by {@link pr1Parser#decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDecl(pr1Parser.DeclContext ctx);
	/**
	 * Visit a parse tree produced by the {@code declVar}
	 * labeled alternative in {@link pr1Parser#decl_var}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclVar(pr1Parser.DeclVarContext ctx);
	/**
	 * Visit a parse tree produced by {@link pr1Parser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType(pr1Parser.TypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code declSub}
	 * labeled alternative in {@link pr1Parser#decl_sub}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclSub(pr1Parser.DeclSubContext ctx);
	/**
	 * Visit a parse tree produced by {@link pr1Parser#sub_head}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSub_head(pr1Parser.Sub_headContext ctx);
	/**
	 * Visit a parse tree produced by {@link pr1Parser#sub_body}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSub_body(pr1Parser.Sub_bodyContext ctx);
	/**
	 * Visit a parse tree produced by {@link pr1Parser#sub_end}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSub_end(pr1Parser.Sub_endContext ctx);
	/**
	 * Visit a parse tree produced by {@link pr1Parser#param_list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParam_list(pr1Parser.Param_listContext ctx);
	/**
	 * Visit a parse tree produced by {@link pr1Parser#param}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParam(pr1Parser.ParamContext ctx);
	/**
	 * Visit a parse tree produced by the {@code stmtRead}
	 * labeled alternative in {@link pr1Parser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStmtRead(pr1Parser.StmtReadContext ctx);
	/**
	 * Visit a parse tree produced by the {@code stmtAssign}
	 * labeled alternative in {@link pr1Parser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStmtAssign(pr1Parser.StmtAssignContext ctx);
	/**
	 * Visit a parse tree produced by the {@code assBool}
	 * labeled alternative in {@link pr1Parser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssBool(pr1Parser.AssBoolContext ctx);
	/**
	 * Visit a parse tree produced by the {@code WBool}
	 * labeled alternative in {@link pr1Parser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWBool(pr1Parser.WBoolContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Ret}
	 * labeled alternative in {@link pr1Parser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRet(pr1Parser.RetContext ctx);
	/**
	 * Visit a parse tree produced by the {@code writeId}
	 * labeled alternative in {@link pr1Parser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWriteId(pr1Parser.WriteIdContext ctx);
	/**
	 * Visit a parse tree produced by the {@code stmtWrite}
	 * labeled alternative in {@link pr1Parser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStmtWrite(pr1Parser.StmtWriteContext ctx);
	/**
	 * Visit a parse tree produced by the {@code stmtAssignId}
	 * labeled alternative in {@link pr1Parser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStmtAssignId(pr1Parser.StmtAssignIdContext ctx);
	/**
	 * Visit a parse tree produced by the {@code doLoop}
	 * labeled alternative in {@link pr1Parser#do_loop}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDoLoop(pr1Parser.DoLoopContext ctx);
	/**
	 * Visit a parse tree produced by {@link pr1Parser#do_head}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDo_head(pr1Parser.Do_headContext ctx);
	/**
	 * Visit a parse tree produced by {@link pr1Parser#do_end}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDo_end(pr1Parser.Do_endContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ifSel}
	 * labeled alternative in {@link pr1Parser#if_sel}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfSel(pr1Parser.IfSelContext ctx);
	/**
	 * Visit a parse tree produced by {@link pr1Parser#if_cond}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIf_cond(pr1Parser.If_condContext ctx);
	/**
	 * Visit a parse tree produced by {@link pr1Parser#if_then}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIf_then(pr1Parser.If_thenContext ctx);
	/**
	 * Visit a parse tree produced by {@link pr1Parser#if_else}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIf_else(pr1Parser.If_elseContext ctx);
	/**
	 * Visit a parse tree produced by {@link pr1Parser#if_end}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIf_end(pr1Parser.If_endContext ctx);
	/**
	 * Visit a parse tree produced by {@link pr1Parser#if_body}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIf_body(pr1Parser.If_bodyContext ctx);
	/**
	 * Visit a parse tree produced by the {@code exprTerm}
	 * labeled alternative in {@link pr1Parser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprTerm(pr1Parser.ExprTermContext ctx);
	/**
	 * Visit a parse tree produced by the {@code exprAddOp}
	 * labeled alternative in {@link pr1Parser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprAddOp(pr1Parser.ExprAddOpContext ctx);
	/**
	 * Visit a parse tree produced by the {@code boolOp}
	 * labeled alternative in {@link pr1Parser#bool}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolOp(pr1Parser.BoolOpContext ctx);
	/**
	 * Visit a parse tree produced by the {@code bT}
	 * labeled alternative in {@link pr1Parser#bool}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBT(pr1Parser.BTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code boolNot}
	 * labeled alternative in {@link pr1Parser#bool}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolNot(pr1Parser.BoolNotContext ctx);
	/**
	 * Visit a parse tree produced by the {@code bF}
	 * labeled alternative in {@link pr1Parser#bool}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBF(pr1Parser.BFContext ctx);
	/**
	 * Visit a parse tree produced by the {@code boolRelOp}
	 * labeled alternative in {@link pr1Parser#bool}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolRelOp(pr1Parser.BoolRelOpContext ctx);
	/**
	 * Visit a parse tree produced by the {@code handleParanthesis}
	 * labeled alternative in {@link pr1Parser#bool}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHandleParanthesis(pr1Parser.HandleParanthesisContext ctx);
	/**
	 * Visit a parse tree produced by the {@code boolId}
	 * labeled alternative in {@link pr1Parser#bool}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolId(pr1Parser.BoolIdContext ctx);
	/**
	 * Visit a parse tree produced by the {@code termMultOp}
	 * labeled alternative in {@link pr1Parser#term}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTermMultOp(pr1Parser.TermMultOpContext ctx);
	/**
	 * Visit a parse tree produced by the {@code termFactor}
	 * labeled alternative in {@link pr1Parser#term}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTermFactor(pr1Parser.TermFactorContext ctx);
	/**
	 * Visit a parse tree produced by the {@code factorExpr}
	 * labeled alternative in {@link pr1Parser#factor}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFactorExpr(pr1Parser.FactorExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code factorId}
	 * labeled alternative in {@link pr1Parser#factor}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFactorId(pr1Parser.FactorIdContext ctx);
	/**
	 * Visit a parse tree produced by the {@code factorIntConst}
	 * labeled alternative in {@link pr1Parser#factor}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFactorIntConst(pr1Parser.FactorIntConstContext ctx);
	/**
	 * Visit a parse tree produced by the {@code factorRealConst}
	 * labeled alternative in {@link pr1Parser#factor}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFactorRealConst(pr1Parser.FactorRealConstContext ctx);
	/**
	 * Visit a parse tree produced by the {@code factorFloat}
	 * labeled alternative in {@link pr1Parser#factor}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFactorFloat(pr1Parser.FactorFloatContext ctx);
	/**
	 * Visit a parse tree produced by the {@code factorTrunc}
	 * labeled alternative in {@link pr1Parser#factor}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFactorTrunc(pr1Parser.FactorTruncContext ctx);
	/**
	 * Visit a parse tree produced by the {@code factorSub}
	 * labeled alternative in {@link pr1Parser#factor}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFactorSub(pr1Parser.FactorSubContext ctx);
	/**
	 * Visit a parse tree produced by {@link pr1Parser#expr_list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr_list(pr1Parser.Expr_listContext ctx);
	/**
	 * Visit a parse tree produced by {@link pr1Parser#add_op}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAdd_op(pr1Parser.Add_opContext ctx);
	/**
	 * Visit a parse tree produced by {@link pr1Parser#mult_op}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMult_op(pr1Parser.Mult_opContext ctx);
}