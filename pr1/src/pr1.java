import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;

import java.io.FileInputStream;
import java.io.IOException;

/**
 * Provides the main class for Homework 6.
 *
 * @author Denis Gracanin
 * @version 1
 */
public class pr1 {
    public static void main(String[] args) throws Exception {
        pr1Parser parser;
        pr1MainVisitor visitor = new pr1MainVisitor();
        boolean inLoop = false;
        String loopString = null;
        if (args.length == 1) {
            try {
                parser = new pr1Parser(new CommonTokenStream(new pr1Lexer(new ANTLRInputStream(new FileInputStream(args[0])))));
                visitor.visit(parser.program());
            } catch (IOException e) {
                System.err.println("Error opening the file " + args[0]);
            }
        }
        else {
            System.err.println("Usage: java hw5 [file_name]");
        }
    }
}
