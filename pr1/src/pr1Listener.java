// Generated from /Users/JishnuRenugopal/IdeaProjects/pr1/pr1.g4 by ANTLR 4.6
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link pr1Parser}.
 */
public interface pr1Listener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link pr1Parser#program}.
	 * @param ctx the parse tree
	 */
	void enterProgram(pr1Parser.ProgramContext ctx);
	/**
	 * Exit a parse tree produced by {@link pr1Parser#program}.
	 * @param ctx the parse tree
	 */
	void exitProgram(pr1Parser.ProgramContext ctx);
	/**
	 * Enter a parse tree produced by {@link pr1Parser#stmt_list}.
	 * @param ctx the parse tree
	 */
	void enterStmt_list(pr1Parser.Stmt_listContext ctx);
	/**
	 * Exit a parse tree produced by {@link pr1Parser#stmt_list}.
	 * @param ctx the parse tree
	 */
	void exitStmt_list(pr1Parser.Stmt_listContext ctx);
	/**
	 * Enter a parse tree produced by {@link pr1Parser#decl}.
	 * @param ctx the parse tree
	 */
	void enterDecl(pr1Parser.DeclContext ctx);
	/**
	 * Exit a parse tree produced by {@link pr1Parser#decl}.
	 * @param ctx the parse tree
	 */
	void exitDecl(pr1Parser.DeclContext ctx);
	/**
	 * Enter a parse tree produced by the {@code declVar}
	 * labeled alternative in {@link pr1Parser#decl_var}.
	 * @param ctx the parse tree
	 */
	void enterDeclVar(pr1Parser.DeclVarContext ctx);
	/**
	 * Exit a parse tree produced by the {@code declVar}
	 * labeled alternative in {@link pr1Parser#decl_var}.
	 * @param ctx the parse tree
	 */
	void exitDeclVar(pr1Parser.DeclVarContext ctx);
	/**
	 * Enter a parse tree produced by {@link pr1Parser#type}.
	 * @param ctx the parse tree
	 */
	void enterType(pr1Parser.TypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link pr1Parser#type}.
	 * @param ctx the parse tree
	 */
	void exitType(pr1Parser.TypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code declSub}
	 * labeled alternative in {@link pr1Parser#decl_sub}.
	 * @param ctx the parse tree
	 */
	void enterDeclSub(pr1Parser.DeclSubContext ctx);
	/**
	 * Exit a parse tree produced by the {@code declSub}
	 * labeled alternative in {@link pr1Parser#decl_sub}.
	 * @param ctx the parse tree
	 */
	void exitDeclSub(pr1Parser.DeclSubContext ctx);
	/**
	 * Enter a parse tree produced by {@link pr1Parser#sub_head}.
	 * @param ctx the parse tree
	 */
	void enterSub_head(pr1Parser.Sub_headContext ctx);
	/**
	 * Exit a parse tree produced by {@link pr1Parser#sub_head}.
	 * @param ctx the parse tree
	 */
	void exitSub_head(pr1Parser.Sub_headContext ctx);
	/**
	 * Enter a parse tree produced by {@link pr1Parser#sub_body}.
	 * @param ctx the parse tree
	 */
	void enterSub_body(pr1Parser.Sub_bodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link pr1Parser#sub_body}.
	 * @param ctx the parse tree
	 */
	void exitSub_body(pr1Parser.Sub_bodyContext ctx);
	/**
	 * Enter a parse tree produced by {@link pr1Parser#sub_end}.
	 * @param ctx the parse tree
	 */
	void enterSub_end(pr1Parser.Sub_endContext ctx);
	/**
	 * Exit a parse tree produced by {@link pr1Parser#sub_end}.
	 * @param ctx the parse tree
	 */
	void exitSub_end(pr1Parser.Sub_endContext ctx);
	/**
	 * Enter a parse tree produced by {@link pr1Parser#param_list}.
	 * @param ctx the parse tree
	 */
	void enterParam_list(pr1Parser.Param_listContext ctx);
	/**
	 * Exit a parse tree produced by {@link pr1Parser#param_list}.
	 * @param ctx the parse tree
	 */
	void exitParam_list(pr1Parser.Param_listContext ctx);
	/**
	 * Enter a parse tree produced by {@link pr1Parser#param}.
	 * @param ctx the parse tree
	 */
	void enterParam(pr1Parser.ParamContext ctx);
	/**
	 * Exit a parse tree produced by {@link pr1Parser#param}.
	 * @param ctx the parse tree
	 */
	void exitParam(pr1Parser.ParamContext ctx);
	/**
	 * Enter a parse tree produced by the {@code stmtRead}
	 * labeled alternative in {@link pr1Parser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterStmtRead(pr1Parser.StmtReadContext ctx);
	/**
	 * Exit a parse tree produced by the {@code stmtRead}
	 * labeled alternative in {@link pr1Parser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitStmtRead(pr1Parser.StmtReadContext ctx);
	/**
	 * Enter a parse tree produced by the {@code stmtAssign}
	 * labeled alternative in {@link pr1Parser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterStmtAssign(pr1Parser.StmtAssignContext ctx);
	/**
	 * Exit a parse tree produced by the {@code stmtAssign}
	 * labeled alternative in {@link pr1Parser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitStmtAssign(pr1Parser.StmtAssignContext ctx);
	/**
	 * Enter a parse tree produced by the {@code assBool}
	 * labeled alternative in {@link pr1Parser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterAssBool(pr1Parser.AssBoolContext ctx);
	/**
	 * Exit a parse tree produced by the {@code assBool}
	 * labeled alternative in {@link pr1Parser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitAssBool(pr1Parser.AssBoolContext ctx);
	/**
	 * Enter a parse tree produced by the {@code WBool}
	 * labeled alternative in {@link pr1Parser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterWBool(pr1Parser.WBoolContext ctx);
	/**
	 * Exit a parse tree produced by the {@code WBool}
	 * labeled alternative in {@link pr1Parser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitWBool(pr1Parser.WBoolContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Ret}
	 * labeled alternative in {@link pr1Parser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterRet(pr1Parser.RetContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Ret}
	 * labeled alternative in {@link pr1Parser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitRet(pr1Parser.RetContext ctx);
	/**
	 * Enter a parse tree produced by the {@code writeId}
	 * labeled alternative in {@link pr1Parser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterWriteId(pr1Parser.WriteIdContext ctx);
	/**
	 * Exit a parse tree produced by the {@code writeId}
	 * labeled alternative in {@link pr1Parser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitWriteId(pr1Parser.WriteIdContext ctx);
	/**
	 * Enter a parse tree produced by the {@code stmtWrite}
	 * labeled alternative in {@link pr1Parser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterStmtWrite(pr1Parser.StmtWriteContext ctx);
	/**
	 * Exit a parse tree produced by the {@code stmtWrite}
	 * labeled alternative in {@link pr1Parser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitStmtWrite(pr1Parser.StmtWriteContext ctx);
	/**
	 * Enter a parse tree produced by the {@code stmtAssignId}
	 * labeled alternative in {@link pr1Parser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterStmtAssignId(pr1Parser.StmtAssignIdContext ctx);
	/**
	 * Exit a parse tree produced by the {@code stmtAssignId}
	 * labeled alternative in {@link pr1Parser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitStmtAssignId(pr1Parser.StmtAssignIdContext ctx);
	/**
	 * Enter a parse tree produced by the {@code doLoop}
	 * labeled alternative in {@link pr1Parser#do_loop}.
	 * @param ctx the parse tree
	 */
	void enterDoLoop(pr1Parser.DoLoopContext ctx);
	/**
	 * Exit a parse tree produced by the {@code doLoop}
	 * labeled alternative in {@link pr1Parser#do_loop}.
	 * @param ctx the parse tree
	 */
	void exitDoLoop(pr1Parser.DoLoopContext ctx);
	/**
	 * Enter a parse tree produced by {@link pr1Parser#do_head}.
	 * @param ctx the parse tree
	 */
	void enterDo_head(pr1Parser.Do_headContext ctx);
	/**
	 * Exit a parse tree produced by {@link pr1Parser#do_head}.
	 * @param ctx the parse tree
	 */
	void exitDo_head(pr1Parser.Do_headContext ctx);
	/**
	 * Enter a parse tree produced by {@link pr1Parser#do_end}.
	 * @param ctx the parse tree
	 */
	void enterDo_end(pr1Parser.Do_endContext ctx);
	/**
	 * Exit a parse tree produced by {@link pr1Parser#do_end}.
	 * @param ctx the parse tree
	 */
	void exitDo_end(pr1Parser.Do_endContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ifSel}
	 * labeled alternative in {@link pr1Parser#if_sel}.
	 * @param ctx the parse tree
	 */
	void enterIfSel(pr1Parser.IfSelContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ifSel}
	 * labeled alternative in {@link pr1Parser#if_sel}.
	 * @param ctx the parse tree
	 */
	void exitIfSel(pr1Parser.IfSelContext ctx);
	/**
	 * Enter a parse tree produced by {@link pr1Parser#if_cond}.
	 * @param ctx the parse tree
	 */
	void enterIf_cond(pr1Parser.If_condContext ctx);
	/**
	 * Exit a parse tree produced by {@link pr1Parser#if_cond}.
	 * @param ctx the parse tree
	 */
	void exitIf_cond(pr1Parser.If_condContext ctx);
	/**
	 * Enter a parse tree produced by {@link pr1Parser#if_then}.
	 * @param ctx the parse tree
	 */
	void enterIf_then(pr1Parser.If_thenContext ctx);
	/**
	 * Exit a parse tree produced by {@link pr1Parser#if_then}.
	 * @param ctx the parse tree
	 */
	void exitIf_then(pr1Parser.If_thenContext ctx);
	/**
	 * Enter a parse tree produced by {@link pr1Parser#if_else}.
	 * @param ctx the parse tree
	 */
	void enterIf_else(pr1Parser.If_elseContext ctx);
	/**
	 * Exit a parse tree produced by {@link pr1Parser#if_else}.
	 * @param ctx the parse tree
	 */
	void exitIf_else(pr1Parser.If_elseContext ctx);
	/**
	 * Enter a parse tree produced by {@link pr1Parser#if_end}.
	 * @param ctx the parse tree
	 */
	void enterIf_end(pr1Parser.If_endContext ctx);
	/**
	 * Exit a parse tree produced by {@link pr1Parser#if_end}.
	 * @param ctx the parse tree
	 */
	void exitIf_end(pr1Parser.If_endContext ctx);
	/**
	 * Enter a parse tree produced by {@link pr1Parser#if_body}.
	 * @param ctx the parse tree
	 */
	void enterIf_body(pr1Parser.If_bodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link pr1Parser#if_body}.
	 * @param ctx the parse tree
	 */
	void exitIf_body(pr1Parser.If_bodyContext ctx);
	/**
	 * Enter a parse tree produced by the {@code exprTerm}
	 * labeled alternative in {@link pr1Parser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExprTerm(pr1Parser.ExprTermContext ctx);
	/**
	 * Exit a parse tree produced by the {@code exprTerm}
	 * labeled alternative in {@link pr1Parser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExprTerm(pr1Parser.ExprTermContext ctx);
	/**
	 * Enter a parse tree produced by the {@code exprAddOp}
	 * labeled alternative in {@link pr1Parser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExprAddOp(pr1Parser.ExprAddOpContext ctx);
	/**
	 * Exit a parse tree produced by the {@code exprAddOp}
	 * labeled alternative in {@link pr1Parser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExprAddOp(pr1Parser.ExprAddOpContext ctx);
	/**
	 * Enter a parse tree produced by the {@code boolOp}
	 * labeled alternative in {@link pr1Parser#bool}.
	 * @param ctx the parse tree
	 */
	void enterBoolOp(pr1Parser.BoolOpContext ctx);
	/**
	 * Exit a parse tree produced by the {@code boolOp}
	 * labeled alternative in {@link pr1Parser#bool}.
	 * @param ctx the parse tree
	 */
	void exitBoolOp(pr1Parser.BoolOpContext ctx);
	/**
	 * Enter a parse tree produced by the {@code bT}
	 * labeled alternative in {@link pr1Parser#bool}.
	 * @param ctx the parse tree
	 */
	void enterBT(pr1Parser.BTContext ctx);
	/**
	 * Exit a parse tree produced by the {@code bT}
	 * labeled alternative in {@link pr1Parser#bool}.
	 * @param ctx the parse tree
	 */
	void exitBT(pr1Parser.BTContext ctx);
	/**
	 * Enter a parse tree produced by the {@code boolNot}
	 * labeled alternative in {@link pr1Parser#bool}.
	 * @param ctx the parse tree
	 */
	void enterBoolNot(pr1Parser.BoolNotContext ctx);
	/**
	 * Exit a parse tree produced by the {@code boolNot}
	 * labeled alternative in {@link pr1Parser#bool}.
	 * @param ctx the parse tree
	 */
	void exitBoolNot(pr1Parser.BoolNotContext ctx);
	/**
	 * Enter a parse tree produced by the {@code bF}
	 * labeled alternative in {@link pr1Parser#bool}.
	 * @param ctx the parse tree
	 */
	void enterBF(pr1Parser.BFContext ctx);
	/**
	 * Exit a parse tree produced by the {@code bF}
	 * labeled alternative in {@link pr1Parser#bool}.
	 * @param ctx the parse tree
	 */
	void exitBF(pr1Parser.BFContext ctx);
	/**
	 * Enter a parse tree produced by the {@code boolRelOp}
	 * labeled alternative in {@link pr1Parser#bool}.
	 * @param ctx the parse tree
	 */
	void enterBoolRelOp(pr1Parser.BoolRelOpContext ctx);
	/**
	 * Exit a parse tree produced by the {@code boolRelOp}
	 * labeled alternative in {@link pr1Parser#bool}.
	 * @param ctx the parse tree
	 */
	void exitBoolRelOp(pr1Parser.BoolRelOpContext ctx);
	/**
	 * Enter a parse tree produced by the {@code handleParanthesis}
	 * labeled alternative in {@link pr1Parser#bool}.
	 * @param ctx the parse tree
	 */
	void enterHandleParanthesis(pr1Parser.HandleParanthesisContext ctx);
	/**
	 * Exit a parse tree produced by the {@code handleParanthesis}
	 * labeled alternative in {@link pr1Parser#bool}.
	 * @param ctx the parse tree
	 */
	void exitHandleParanthesis(pr1Parser.HandleParanthesisContext ctx);
	/**
	 * Enter a parse tree produced by the {@code boolId}
	 * labeled alternative in {@link pr1Parser#bool}.
	 * @param ctx the parse tree
	 */
	void enterBoolId(pr1Parser.BoolIdContext ctx);
	/**
	 * Exit a parse tree produced by the {@code boolId}
	 * labeled alternative in {@link pr1Parser#bool}.
	 * @param ctx the parse tree
	 */
	void exitBoolId(pr1Parser.BoolIdContext ctx);
	/**
	 * Enter a parse tree produced by the {@code termMultOp}
	 * labeled alternative in {@link pr1Parser#term}.
	 * @param ctx the parse tree
	 */
	void enterTermMultOp(pr1Parser.TermMultOpContext ctx);
	/**
	 * Exit a parse tree produced by the {@code termMultOp}
	 * labeled alternative in {@link pr1Parser#term}.
	 * @param ctx the parse tree
	 */
	void exitTermMultOp(pr1Parser.TermMultOpContext ctx);
	/**
	 * Enter a parse tree produced by the {@code termFactor}
	 * labeled alternative in {@link pr1Parser#term}.
	 * @param ctx the parse tree
	 */
	void enterTermFactor(pr1Parser.TermFactorContext ctx);
	/**
	 * Exit a parse tree produced by the {@code termFactor}
	 * labeled alternative in {@link pr1Parser#term}.
	 * @param ctx the parse tree
	 */
	void exitTermFactor(pr1Parser.TermFactorContext ctx);
	/**
	 * Enter a parse tree produced by the {@code factorExpr}
	 * labeled alternative in {@link pr1Parser#factor}.
	 * @param ctx the parse tree
	 */
	void enterFactorExpr(pr1Parser.FactorExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code factorExpr}
	 * labeled alternative in {@link pr1Parser#factor}.
	 * @param ctx the parse tree
	 */
	void exitFactorExpr(pr1Parser.FactorExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code factorId}
	 * labeled alternative in {@link pr1Parser#factor}.
	 * @param ctx the parse tree
	 */
	void enterFactorId(pr1Parser.FactorIdContext ctx);
	/**
	 * Exit a parse tree produced by the {@code factorId}
	 * labeled alternative in {@link pr1Parser#factor}.
	 * @param ctx the parse tree
	 */
	void exitFactorId(pr1Parser.FactorIdContext ctx);
	/**
	 * Enter a parse tree produced by the {@code factorIntConst}
	 * labeled alternative in {@link pr1Parser#factor}.
	 * @param ctx the parse tree
	 */
	void enterFactorIntConst(pr1Parser.FactorIntConstContext ctx);
	/**
	 * Exit a parse tree produced by the {@code factorIntConst}
	 * labeled alternative in {@link pr1Parser#factor}.
	 * @param ctx the parse tree
	 */
	void exitFactorIntConst(pr1Parser.FactorIntConstContext ctx);
	/**
	 * Enter a parse tree produced by the {@code factorRealConst}
	 * labeled alternative in {@link pr1Parser#factor}.
	 * @param ctx the parse tree
	 */
	void enterFactorRealConst(pr1Parser.FactorRealConstContext ctx);
	/**
	 * Exit a parse tree produced by the {@code factorRealConst}
	 * labeled alternative in {@link pr1Parser#factor}.
	 * @param ctx the parse tree
	 */
	void exitFactorRealConst(pr1Parser.FactorRealConstContext ctx);
	/**
	 * Enter a parse tree produced by the {@code factorFloat}
	 * labeled alternative in {@link pr1Parser#factor}.
	 * @param ctx the parse tree
	 */
	void enterFactorFloat(pr1Parser.FactorFloatContext ctx);
	/**
	 * Exit a parse tree produced by the {@code factorFloat}
	 * labeled alternative in {@link pr1Parser#factor}.
	 * @param ctx the parse tree
	 */
	void exitFactorFloat(pr1Parser.FactorFloatContext ctx);
	/**
	 * Enter a parse tree produced by the {@code factorTrunc}
	 * labeled alternative in {@link pr1Parser#factor}.
	 * @param ctx the parse tree
	 */
	void enterFactorTrunc(pr1Parser.FactorTruncContext ctx);
	/**
	 * Exit a parse tree produced by the {@code factorTrunc}
	 * labeled alternative in {@link pr1Parser#factor}.
	 * @param ctx the parse tree
	 */
	void exitFactorTrunc(pr1Parser.FactorTruncContext ctx);
	/**
	 * Enter a parse tree produced by the {@code factorSub}
	 * labeled alternative in {@link pr1Parser#factor}.
	 * @param ctx the parse tree
	 */
	void enterFactorSub(pr1Parser.FactorSubContext ctx);
	/**
	 * Exit a parse tree produced by the {@code factorSub}
	 * labeled alternative in {@link pr1Parser#factor}.
	 * @param ctx the parse tree
	 */
	void exitFactorSub(pr1Parser.FactorSubContext ctx);
	/**
	 * Enter a parse tree produced by {@link pr1Parser#expr_list}.
	 * @param ctx the parse tree
	 */
	void enterExpr_list(pr1Parser.Expr_listContext ctx);
	/**
	 * Exit a parse tree produced by {@link pr1Parser#expr_list}.
	 * @param ctx the parse tree
	 */
	void exitExpr_list(pr1Parser.Expr_listContext ctx);
	/**
	 * Enter a parse tree produced by {@link pr1Parser#add_op}.
	 * @param ctx the parse tree
	 */
	void enterAdd_op(pr1Parser.Add_opContext ctx);
	/**
	 * Exit a parse tree produced by {@link pr1Parser#add_op}.
	 * @param ctx the parse tree
	 */
	void exitAdd_op(pr1Parser.Add_opContext ctx);
	/**
	 * Enter a parse tree produced by {@link pr1Parser#mult_op}.
	 * @param ctx the parse tree
	 */
	void enterMult_op(pr1Parser.Mult_opContext ctx);
	/**
	 * Exit a parse tree produced by {@link pr1Parser#mult_op}.
	 * @param ctx the parse tree
	 */
	void exitMult_op(pr1Parser.Mult_opContext ctx);
}