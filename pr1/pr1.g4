grammar pr1;

program : stmt_list ;

stmt_list
    : stmt_list decl
    | stmt_list stmt
    | stmt_list do_loop
    | stmt_list if_sel
    |
    ;

decl
    : decl_var
    | decl_sub
    ;

decl_var
    : type ID                   #declVar
    ;

type
    : 'int'
    | 'real'
    | 'bool'
    ;

decl_sub
    : sub_head sub_body sub_end #declSub
    ;

sub_head
    : type 'SUB' ID '(' param_list ')'
    ;

sub_body
    : sub_body (decl_var | stmt | if_sel | do_loop )
    |
    ;

sub_end
    : 'ENDSUB'
    ;

param_list
    : (param (',' param)* )?
    ;

param
    : type ID
    ;

stmt
    :
     'read' ID                 #stmtRead
   | ID ':=' expr              #stmtAssign
       | ID ':=' bool         #assBool
    | 'write' bool         #WBool
    | 'RETURN' expr             #Ret
     | 'write' ID                #writeId
        | 'write' expr              #stmtWrite
    |a=ID ':=' b=ID        #stmtAssignId
    ;

do_loop
    : do_head stmt_list do_end    #doLoop
    ;

do_head
    : 'DO' ID '=' INT_CONST ',' INT_CONST (',' INT_CONST)?
    ;



do_end
    : 'ENDDO'
    ;

if_sel
    : if_cond if_then if_else? if_end   #ifSel
    ;

if_cond
    : 'IF' bool
    ;

if_then
    : 'THEN' if_body
    ;

if_else
    : 'ELSE' if_body
    ;

if_end
    : 'ENDIF'
    ;

if_body
    : if_body (stmt | do_loop )
    |
    ;

expr returns [Number value]
    : term                      #exprTerm
    | expr add_op term          #exprAddOp
    ;

bool returns [Boolean value]
    : ID                                                   #boolId
    | 'true'                                               #bT
    | 'false'                                              #bF
    | '(' bool ')'                                    #handleParanthesis
    | a=bool BOP b=bool              #boolOp
     | e1=expr REL e2=expr                         #boolRelOp
        | 'NOT' bool                                      #boolNot
    ;



term returns [Number value]
    : factor                    #termFactor
    | term mult_op factor       #termMultOp
    ;

factor returns [Number value]
    : '(' expr ')'              #factorExpr
    | ID                        #factorId
    | INT_CONST                 #factorIntConst
    | REAL_CONST                #factorRealConst
    | 'float' '(' expr ')'      #factorFloat
    | 'trunc' '(' expr ')'      #factorTrunc
    | ID '(' expr_list ')'      #factorSub
    ;

expr_list
    : (expr (',' expr)* )?
    ;

add_op
    : '+'
    | '-'
    ;

mult_op
    : '*'
    | '/'
    ;

BOP :  'OR'|'AND'  ;

REL : '<' | '<=' | '==' | '!=' | '>=' | '>' ;

INT_CONST : '-'? POSITIVE_CONSTANT ;

REAL_CONST : INT_CONST EXPONENT | DECIMAL ( EXPONENT | ) ;

fragment POSITIVE_CONSTANT : ('0' | DIGITNZ DIGIT*) ;

fragment DECIMAL : INT_CONST '.' DIGIT* | '.' DIGIT+ ;

fragment EXPONENT : ( 'e' | 'E' ) ( '+' | '-' | ) POSITIVE_CONSTANT ;

fragment DIGITNZ : '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' ;

fragment DIGIT : DIGITNZ | '0' ;

ID : [a-zA-Z][a-zA-Z0-9_]* ;

WS : [ \t\r\n]+ -> skip ;
