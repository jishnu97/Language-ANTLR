**Data types supported**
* Boolean
* Integers
* Real numbers


**Operations supported**
* Modulo
* Multiplication
* Division
* Addition
* Subtraction
* Increment
* Division


**Tools used**
* ANTLR - for grammar
* Visitor functions with Java - to handle operations


**Control structures supported**
* If
* While
* For
